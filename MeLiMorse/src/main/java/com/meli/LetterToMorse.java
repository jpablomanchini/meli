package com.meli;

import java.util.HashMap;

public class LetterToMorse {
	private final HashMap letterCode = new HashMap();
	
	public LetterToMorse() {
		super();
		// TODO Auto-generated constructor stub
		letterCode.put(".-", "A");
		letterCode.put("-...", "B");
		letterCode.put("-.-.", "C");
		letterCode.put("-..", "D");
		letterCode.put(".", "E");
		letterCode.put("..-.", "F");
		letterCode.put("--.", "G");
		letterCode.put("....", "H");
		letterCode.put("..", "I");
		letterCode.put(".---", "J");
		letterCode.put("-.-", "K");
		letterCode.put(".-..", "L");
		letterCode.put("--", "M");
		letterCode.put("-.", "N");
		letterCode.put("---", "O");
		letterCode.put(".--.", "P");
		letterCode.put("--.-", "Q");
		letterCode.put(".-.", "R");
		letterCode.put("...", "S");
		letterCode.put("-", "T");
		letterCode.put("..-", "U");
		letterCode.put("...-", "V");
		letterCode.put(".--", "W");
		letterCode.put("-..-", "X");
		letterCode.put("-.--", "Y");
		letterCode.put("--..", "Z");
		letterCode.put("-----", "0");
		letterCode.put(".----", "1");
		letterCode.put("..---", "2");
		letterCode.put("...--", "3");
		letterCode.put("....-", "4");
		letterCode.put(".....", "5");
		letterCode.put("-....", "6");
		letterCode.put("--...", "7");
		letterCode.put("---..", "8");
		letterCode.put("----.", "9");
		letterCode.put(".-.-.-", "FS");
	}

	public HashMap getLetterCode() {
		return this.letterCode;
	}	
}
