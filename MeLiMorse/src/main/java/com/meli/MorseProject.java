package com.meli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@WebServlet(name = "meLi", 
	urlPatterns = { "/translate/2text", "/translate/2morse", "/translate/bits2morse" })

public class MorseProject extends HttpServlet {

	private final static String MIN = "min";	
	private final static String MAX = "max";
	private final static float percent = (float) 0.6;
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		response.setContentType("text/plain");
    	response.setCharacterEncoding("UTF-8");
	    
	    try {            
	    	String input = request.getParameter("text");
        	if(input == null) {
            	response.getWriter().write("Ingrese cadena a traducir");
            } else {
            	String result = "";
            	
            	if(request.getServletPath().compareTo("/translate/2text") == 0) {
            		result = this.translate2Human(input);            		
            	} else if(request.getServletPath().compareTo("/translate/bits2morse") == 0) {
            		result = this.decodeBits2Morse(input);            		
            	} else if(request.getServletPath().compareTo("/translate/2morse") == 0) {
            		result = this.translate2Morse(input);            		
            	}
            	 
            	response.getWriter().print("{code: " + response.getStatus() + ", " + "response:'" + result + "'}");
            }
        } catch (Exception e) {
        	response.setStatus(400);
        	response.getWriter().print("{code: " + response.getStatus() + ", " + "response:'null'}");
        }
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
    	response.setCharacterEncoding("UTF-8");
    	
    	PrintWriter printOut = response.getWriter();
    	
		try {
			String input = request.getParameter("text");
			
			if(input == null) {
				input = this.jsonTransform(request);
            } 

           	String result = "";
           	
           	if(request.getServletPath().compareTo("/translate/2text") == 0) {
           		result = this.translate2Human(input);            		
           	} else if(request.getServletPath().compareTo("/translate/bits2morse") == 0) {
           		result = this.decodeBits2Morse(input);            		
           	} else if(request.getServletPath().compareTo("/translate/2morse") == 0) {
           		result = this.translate2Morse(input);            		
           	}
            	
           	printOut.println("{code: " + response.getStatus() + ", " + "response:'" + result + "'}");
           	printOut.close();
		} catch (Exception e) {
        	response.setStatus(400);
        	response.getWriter().print("{code: " + response.getStatus() + ", " + "response:'null'}");
        }
	}
	
	public String jsonTransform(HttpServletRequest request) throws Exception{
    	StringBuffer jb = new StringBuffer();
    	String line = null;

    	BufferedReader reader = request.getReader();
    	while ((line = reader.readLine()) != null)
    		jb.append(line);

    	JsonParser parser = new JsonParser();
    	JsonElement json = parser.parse(jb.toString());
    	JsonObject object=json.getAsJsonObject();
    	
    	String input = object.get("text") != null ? object.get("text").toString().replaceAll("\"","") : null;
    	
    	return input;
    }

	public String translate2Morse(String stringMorse) {
		MorseToLetter morseToLetter = new MorseToLetter();
		HashMap morseCode = morseToLetter.getMorseCode();
				
		String traduccion = "";
		boolean notEmpty = true;
		String letra = "";
		try {
			while (notEmpty) {
				letra = stringMorse.substring(0, 1);
				
				if (stringMorse.length() > 1) {
					stringMorse = stringMorse.substring(1, stringMorse.length());
				} else {
					notEmpty = false;
				}

				if (letra.compareTo(" ") == 0) {
					traduccion += " ";
				} else {
					traduccion += morseCode.get(letra) + " ";
				}
			}
			
		} catch (Exception e) {
			System.out.println("Se produjo un error en el codigo ingresado");
		}
		return traduccion;
	}
	
	public String translate2Human(String stringMorse) {
		LetterToMorse letterToMorse = new LetterToMorse();
		HashMap letterCode = letterToMorse.getLetterCode();
		
		String traduccion = "";
		boolean notEmpty = true;
		String letraMorse = "";
		try {
			while (notEmpty) {
				if (stringMorse.indexOf(" ") != -1) {
					letraMorse = stringMorse.substring(0, stringMorse.indexOf(" "));
					
					if(letterCode.get(letraMorse) == letterCode.get(".-.-.-")) {
						letraMorse = "";
						notEmpty = false;
					}
				} else {
					letraMorse = stringMorse.substring(0, stringMorse.length());
					notEmpty = false;
				}

				if (letraMorse.isEmpty()) {
					traduccion += " ";
				} else {
					traduccion += letterCode.get(letraMorse);
				}

				stringMorse = stringMorse.substring(stringMorse.indexOf(" ") + 1, stringMorse.length());
			}
			
		} catch (Exception e) {
			System.out.println("Se produjo un error en el codigo ingresado");
		}
		return traduccion;
	}
	
	private String decodeBits2Morse(String stringBits) {

		//Elimino 0s iniciales y finales
		String cadena = stringBits.substring(stringBits.indexOf("1"), stringBits.lastIndexOf("1") + 1);

		Integer[] limits = calcMaxMin(cadena);

		String caracter = "", subcadena = "", morseCode = "";

		while (!cadena.isEmpty() && cadena != "") {
			caracter = cadena.substring(0, 1);

			if (caracter.compareTo("0") == 0) {
				if (cadena.contains("1")) {
					subcadena = cadena.substring(cadena.indexOf("0"), cadena.indexOf("1"));
					cadena = cadena.substring(cadena.indexOf("1"), cadena.length());
				} else {
					subcadena = cadena;
					cadena = "";
				}

				if (subcadena.length() > (limits[1] - limits[0]) * this.percent) {
					morseCode += " ";
				}

			} else if (caracter.compareTo("1") == 0) {
				if (cadena.contains("0")) {
					subcadena = cadena.substring(cadena.indexOf("1"), cadena.indexOf("0"));
					cadena = cadena.substring(cadena.indexOf("0"), cadena.length());
				} else {
					subcadena = cadena;
					cadena = "";
				}

				if (subcadena.length() <= (limits[3] - limits[2]) * this.percent) {
					morseCode += ".";
				} else if (subcadena.length() > (limits[3] - limits[2]) * this.percent) {
					morseCode += "-";
				}
			}
		}
		return morseCode;
		
	}

	//Obtengo cantidad Maxima y Minima de 0s y 1s consecutivos  
	public Integer[] calcMaxMin(String cadenaBits) {
		String cadena = cadenaBits;
		String caracter = "", subcadena = "", morseCode = "";
		
		Integer[] limits = new Integer[4];
		//limites[0]=min0;
		limits[0]=0;
		//limites[1]=max0;
		limits[1]=0;
		//limites[2]=min1;
		limits[2]=0;
		//limites[3]=max1;
		limits[3]=0;
		
		while (!cadena.isEmpty()) {
			caracter = cadena.substring(0, 1);
						
			if (caracter.compareTo("0") == 0) {
				if (cadena.contains("1")) {
					subcadena = cadena.substring(cadena.indexOf("0"), cadena.indexOf("1"));
					cadena = cadena.substring(cadena.indexOf("1"), cadena.length());
				} else {
					subcadena = cadena;
					cadena = "";
				}
				
				if(subcadena.length() < limits[0] || limits[0] == 0)
					limits[0] = subcadena.length();
				if(subcadena.length() > limits[1] || limits[1] == 0)
					limits[1] = subcadena.length();
			} else if (caracter.compareTo("1") == 0) {
				if (cadena.contains("0")) {
					subcadena = cadena.substring(cadena.indexOf("1"), cadena.indexOf("0"));
					cadena = cadena.substring(cadena.indexOf("0"), cadena.length());
				} else {
					subcadena = cadena;
					cadena = "";
				}
				
				if(subcadena.length() < limits[2] || limits[2] == 0)
					limits[2] = subcadena.length();
				if(subcadena.length() > limits[3] || limits[3] == 0)
					limits[3] = subcadena.length();
			}
		}

		return limits;
	}
}